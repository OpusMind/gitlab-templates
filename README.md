# GitLab CI/CD Scenarios

## Usage

Create an empty `.gitlab-ci.yml` file in your project. Scenarios have a hierarchical organization.
Include an appropriate base scenario using the `include` command.
For example, for a PHP library, you can do the following:

```yaml
include:
  - project: opusmind/gitlab-tools
    file:
      - gitlab-ci-library.yml
```

If necessary, you can add your own tasks to the file or include ready-made ones from the library.

## PHP

To control the build, you can change the following variables.

| Name             | Default | Description                                                               | Where Defined        |
|------------------|---------|---------------------------------------------------------------------------|----------------------|
| `PHP_VERSION`    | `8.2`   | PHP version in the format `'x.y'`.                                        | [variables.yml](variables.yml) |
| `PHP_EXTENSIONS` |         | List of additional PHP extensions separated by spaces, e.g.: `'xsl zip'`. | [variables.yml](variables.yml) |

### PHP Libraries

For building PHP libraries, use [gitlab-ci-library.yml](gitlab-ci-library.yml) as the main scenario.

## Jobs

In the **[jobs](jobs)** folder, you can find job descriptions that can be included in other files using the `include` command.

## Templates

In the **[templates](templates)** folder, you can find task templates that can be `extended` to create your tasks based on them.

For example, to build OpenAPI documentation, you can use the template
[templates/openapi.yml](templates/openapi.yml):

```yaml
include:
  - project: opusmind/gitlab-tools
    file:
      - gitlab-ci-service.yml
      - templates/openapi.yml

docs:http:v1:
  extends:
    - .openapi-docs
  variables:
    NAME: v1
```

### Build Stages

All builds use a common set of possible stages. This allows creating optionally included
job files that run at a specific stage.

In different types of projects, some stages may be absent.

| Stage    | Description        |
|----------|--------------------|
| `test`   | Testing            |
| `build`  | Build              |
| `post`   | Post-build Actions |
| `deploy` | Deployment         |